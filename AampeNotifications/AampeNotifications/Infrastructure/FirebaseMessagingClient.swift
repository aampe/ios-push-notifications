//
//  FirebaseClient.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 26/12/23.
//

import Foundation
import FirebaseCore
import FirebaseMessaging

class FirebaseMessagingClient: NSObject {
    static let share = FirebaseMessagingClient()
    
    /// Configure firebase app.
    /// Before configuring you need to add `GoogleService-Info.plist` file into your project.
    func configure() {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
    }
    
    /// Set APNS token to configure push notification 
    /// - Parameter token: `Data` to define notification device token received from APNS server.
    func setApnsToken(_ token: Data) {
        Messaging.messaging().apnsToken = token
    }
    
    /// Received push notifcation message with use
    /// - Parameter userInfo: `[AnyHashable: Any]` to define notification payload.
    func messageDidReceived(userInfo: [AnyHashable: Any]) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
}

// MARK: Firebase Messaging Deleagte
extension FirebaseMessagingClient: MessagingDelegate {
    
    /// This callback is fired at each app startup and whenever a new token is generated.
    /// If necessary send token to application server.
    /// - Parameter messaging: `Messaging` to define message object.
    /// - Parameter fcmToken: `String?` to define FCM push notification token. That is used to send push message to client. You can save this token to own server to future use.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken else { return }
        print("Firebase registration token: \(String(describing: fcmToken))")
        /// Save APNS token on the firestore database. Please not  Firestore should be enabled in the `native` mode before try to save token.
        Task {
            await FirebaseService.shared.putFcmToken(token: fcmToken)
        }
    }
}
