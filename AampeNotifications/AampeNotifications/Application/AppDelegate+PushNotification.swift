//
//  AppDelegate+PushNotification.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 26/12/23.
//

import UIKit
import UserNotifications

extension AppDelegate {

    /// Register for push notification.
    /// This method should be called from application didFinishLaunchingWithOptions delegate method.
    /// - Parameter application: `UIApplication` to define the notification auth options.
    /// - Parameter options: `UNAuthorizationOptions` to define the notification auth options. For eg. [.alert, .badge, .sound]
    func registerForPushNotification(
        application: UIApplication,
        options: UNAuthorizationOptions = [.alert, .badge, .sound]
    ) {        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        
        // Request for push notification authorization.
        notificationCenter.requestAuthorization(options: options) { authorized, error in
            if authorized {
                DispatchQueue.main.async {
                    
                    let cat1 = self.category1()
                    let cat2 = self.category2()
                    UNUserNotificationCenter.current().setNotificationCategories(Set([cat1, cat2]))
                    
                    application.registerForRemoteNotifications()
                }
            }
        }
    }
    private func category1() -> UNNotificationCategory {
        let yesAction = UNNotificationAction(identifier: "yes_action", title: NSLocalizedString("Yeah!", comment: "Yeah"), options: UNNotificationActionOptions.foreground)
        let maybeAction = UNNotificationAction(identifier: "maybe_action", title: NSLocalizedString("Meh!", comment: "Meh!"), options: UNNotificationActionOptions.foreground)
        let nopeAction = UNNotificationAction(identifier: "nope_action", title: NSLocalizedString("Nope!", comment: "Nope"), options: UNNotificationActionOptions.foreground)
        
        return UNNotificationCategory(identifier: "action-notification", actions: [yesAction, maybeAction, nopeAction], intentIdentifiers: [], options: [])
    }
    private func category2() -> UNNotificationCategory {
        let bookAction = UNNotificationAction(identifier: "book_action", title: NSLocalizedString("Book", comment: "book"), options: UNNotificationActionOptions.foreground)
        let nopeAction = UNNotificationAction(identifier: "nope_action", title: NSLocalizedString("Nope!", comment: "Nope"), options: UNNotificationActionOptions.foreground)
        
        return UNNotificationCategory(identifier: "cab-notification", actions: [bookAction, nopeAction], intentIdentifiers: [], options: [])
    }
}

// MARK: - User Notification Center Delegate
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    /// UNUserNotificationCenterDelegate method to provide device token if user allow the permission.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FirebaseMessagingClient.share.setApnsToken(deviceToken)
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
    }
    
    /// UNUserNotificationCenterDelegate method to fail if user don't allow or has any OS limitations.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for push notification with error:", error)
    }
    
    /// Method to use display received notification in if your app is in active state.
    /// If you don't want to show system notification alert in active state then please remove this methos.
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        let userInfo = notification.request.content.userInfo

        /// Use for Analytics.
        /// If you don't want any Analytics in notification then please remove below code.
        FirebaseMessagingClient.share.messageDidReceived(userInfo: userInfo)
        
        /// Confirm system to display banner with following values.
        completionHandler([.banner, .badge, .sound])
    }
    
    /*! The method will be called on the delegate when the user responded to the notification by opening the application, dismissing the notification or choosing a UNNotificationAction. The delegate must be set before the application returns from application:didFinishLaunchingWithOptions: !*/
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        let userInfo = response.notification.request.content.userInfo
        
        /// Use for Analytics.
        /// If you don't want any Analytics in notification then please remove below code.
        FirebaseMessagingClient.share.messageDidReceived(userInfo: userInfo)
        
        /// Handle push notification tap action
        Router.handlePushNotificationAction(with: response)

        completionHandler()
    }

    /*! This delegate method offers an opportunity for applications with the "remote-notification" background mode to fetch appropriate new data in response to an incoming remote notification. You should call the fetchCompletionHandler as soon as you're finished performing that operation, so the system can accurately estimate its power and data cost.

     This method will be invoked even if the application was launched or resumed because of the remote notification. The respective delegate methods will be invoked first. Note that this behavior is in contrast to application:didReceiveRemoteNotification:, which is not called in those cases, and which will not be invoked if this method is implemented. !*/
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
    ) {
        /// Use for Analytics.
        /// If you don't want any Analytics in notification then please remove below code.
        FirebaseMessagingClient.share.messageDidReceived(userInfo: userInfo)

        completionHandler(.noData)
    }
}
