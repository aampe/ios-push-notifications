//
//  FirebaseService.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 12/01/24.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift
import Foundation

enum Collections: String {
    case users = "users"
}

/// Very important - to use this class we use Firestore in the native mode. NOT in the cloud Firestore mode.
class FirebaseService {
    static let shared = FirebaseService()
    let database = Firestore.firestore()
    
    
    /// post function is used to created a new document(node) on the firebase database
    /// - Parameters:
    ///   - value: any value (firebase token in our case) to store on the firestore
    ///   - collection: a path (node) on the firestore
    /// - Returns: return error or success
    func post<T: FirebaseIdentifiable>(_ value: T, to collection: Collections) async -> Result<T, Error> {
        let ref = database.collection(collection.rawValue).document()
        var valueToWrite: T = value
        valueToWrite.id = ref.documentID
        do {
            try ref.setData(from: valueToWrite)
            return .success(valueToWrite)
        } catch let error {
            print("Error: \(#function) in collection: \(collection), \(error)")
            return .failure(error)
        }
    }
    
    /// put function is used to update the firebase token under user's uid
    /// - Parameters:
    ///   - value: any value (firebase token in our case) to store on the firestore
    ///   - collection: a path (node) on the firestore
    /// - Returns: return error or success
    func put<T: FirebaseIdentifiable>(_ value: T, to collection: Collections) async -> Result<T, Error> {
        let ref = database.collection(collection.rawValue).document(value.id)
        do {
            try ref.setData(from: value)
            return .success(value)
        } catch let error {
            print("Error: \(#function) in \(collection) for id: \(value.id), \(error)")
            return .failure(error)
        }
    }

    
    /// A helper functio to store firebase toien
    /// - Parameter token: firebase token
    func putFcmToken(token: String) async {
        guard let user = Auth.auth().currentUser else { return }
        _ = await put(
            User(
                id: user.uid,
                pushToken: token,
                name: user.displayName ?? "",
                email: user.email ?? ""
            ), to: .users)
    }
}
