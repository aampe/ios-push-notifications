//
//  AuthviewModel.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 12/01/24.
//

import Foundation
import FirebaseAuth
import FirebaseCore
import GoogleSignIn

enum FirebaseAuthMethod {
    case signIn
}
@MainActor
class AuthviewModel {
    private var authManager = AuthManager()
    
    func googleSignIn(method:FirebaseAuthMethod = .signIn) async throws {
        
        // 1
        if GIDSignIn.sharedInstance.hasPreviousSignIn() {
            
           try await restorePreviousGoogleSignIn(method: method)
        } else {
            // 2
            guard let clientID = FirebaseApp.app()?.options.clientID else { throw SignInError.failedToSignIn("Login failed") }
            
            // Create Google Sign In configuration object.
            let config = GIDConfiguration(clientID: clientID)
            GIDSignIn.sharedInstance.configuration = config
            
            // 3
            guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene else { throw SignInError.failedToSignIn("Login failed") }
            guard let rootViewController = windowScene.windows.first?.rootViewController else { throw SignInError.failedToSignIn("Login failed") }
            
            // 4
            let result = try await GIDSignIn.sharedInstance.signIn(withPresenting: rootViewController)
            let user = result.user
            try await googleAuthenticate(for: user, using: method)
            
        }
    }
    
    private func restorePreviousGoogleSignIn(method:FirebaseAuthMethod) async throws {
        let user = try await GIDSignIn.sharedInstance.restorePreviousSignIn()
        try await googleAuthenticate(for: user, using: method)
    }
    
    private func googleAuthenticate(for user: GIDGoogleUser, using method:FirebaseAuthMethod) async throws {
        
        guard let idToken = user.idToken?.tokenString else { return }
        let accessToken = user.accessToken.tokenString
        
        let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
        try await self.performFirebaseOperation(method: method, credential: credential)
    }
    
    func performFirebaseOperation(method: FirebaseAuthMethod, credential:AuthCredential) async throws {
        switch method {
        case .signIn:
            try await authManager.signInWithCredential(credential: credential)
        }
    }

    func signout() async {
        do {
            try await authManager.signout()
        } catch {
            print("error")
        }
    }
    
    func guestLogin() async {
        do {
            try await authManager.guestLogin()
        } catch {
            print("error")
        }
    }
}

class AuthManager {
    init() {
        Auth.auth().useAppLanguage()
    }

    static var isLoggedIn: Bool {
        Auth.auth().currentUser != nil
    }

    func signInWithCredential(credential: AuthCredential) async throws {
        try await Auth.auth().signIn(with: credential)
    }
    
    func guestLogin() async throws {
        try await Auth.auth().signInAnonymously()
    }
    
    func signout() async throws {
        try Auth.auth().signOut()
    }

    static func setUserForAnalytics() {
        guard let user = Auth.auth().currentUser else { return }
        FirebaseAnalyticsClient.setUser(
            id: user.uid,
            name: user.displayName ?? "",
            email: user.email ?? ""
        )
    }
}

enum SignInError: Error {
    case failedToSignIn(String)
}
