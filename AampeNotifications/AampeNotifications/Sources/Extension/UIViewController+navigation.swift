//
//  UIViewController+navigation.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 12/01/24.
//

import Foundation
import UIKit

extension UIViewController {
    func push<T: UIViewController>(to storyboard: Storyboard, type: T.Type) {
        let vc: T = .create(with: storyboard)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func present<T: UIViewController>(to storyboard: Storyboard, type: T.Type) {
        let vc: T = .create(with: storyboard)
        present(vc, animated: true)
    }
}

enum Storyboard: String {
    case main = "Main"

    var storyboard: UIStoryboard {
        UIStoryboard(name: self.rawValue, bundle: nil)
    }
}

extension UIViewController {
    class func create<T: UIViewController>(with storybord: Storyboard) -> T {
        let identifier = String(describing: self)
        guard let viewController = storybord.storyboard.instantiateViewController(
            withIdentifier: identifier
        ) as? T else {
            preconditionFailure("Expected view controller with identifier \(identifier) to be of type \(self)")
        }
        return viewController
    }
}

extension UIViewController {
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

extension UIWindow {
    func switchRootViewController(
        _ viewController: UIViewController,
        animated: Bool = true,
        duration: TimeInterval = 0.5,
        options: AnimationOptions = .transitionFlipFromRight,
        completion: (() -> Void)? = nil
    ) {
        guard animated else {
            rootViewController = viewController
            return
        }
        
        UIView.transition(with: self, duration: duration, options: options, animations: {
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            let navigationController = UINavigationController(rootViewController: viewController)
            self.rootViewController = navigationController
            UIView.setAnimationsEnabled(oldState)
        }, completion: { _ in
            completion?()
        })
    }
    
}
