//
//  Router+PushNotification.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import Foundation
import UserNotifications

extension Router {
    static func handlePushNotificationAction(with response: UNNotificationResponse) {
        let userInfo = response.notification.request.content.userInfo
        guard let link = PushNotificationContentLink(userInfo: userInfo) else { return }

        /// Log firebase analytics events
        FirebaseAnalyticsClient.logEvent(.screenLink(
            name: link.contentName ?? "",
            contentId: link.contentId ?? "",
            action: response.actionIdentifier
        ))

        switch link.type {
        case .screen:
            Router.handlePushNotificationScreenLink(link.screenLink())
        case .openOtherApp:
            break
        }
    }

    private static func handlePushNotificationScreenLink(_ link: PushNotificationScreenLink?) {
        guard let link else { return }
        switch link {
        case .product(let id):
            let viewController: ProductViewController = .create(with: .main)
            viewController.contentId = id
            push(viewController)
        }
    }
}
