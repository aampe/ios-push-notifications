//
//  ViewController.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 25/12/23.
//

import UIKit

class HomeViewController: UIViewController {
    private let authViewModel = AuthviewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    @IBAction private func signOutTapped(_ sender: Any) {
        Task {
            await authViewModel.signout()
            Router.loadInitialScreen(isLoggedIn: false)
        }
    }
}

