//
//  SignInViewController.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 12/01/24.
//

import UIKit

class SignInViewController: UIViewController {
    @IBOutlet private weak var activity: UIActivityIndicatorView!
    private let authViewModel = AuthviewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        activity.isHidden = true
    }
    
    ///  User auth flow -- login using google account
    @IBAction private func signInTapped() {
        activity.isHidden = false
        activity.startAnimating()
        Task {
            do {
                try await authViewModel.googleSignIn()
                AuthManager.setUserForAnalytics()
                moveNext()
            } catch {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                activity.stopAnimating()
            }
            
        }
    }
    
    /// Guest user flow
    @IBAction private func guestUserTapped() {
        activity.isHidden = false
        activity.startAnimating()
        Task {
            await authViewModel.guestLogin()
            moveNext()
        }
    }
    
    private func moveNext() {
        activity.stopAnimating()
        Router.loadInitialScreen(isLoggedIn: true)
        
        /// Register Push notification
        appDelegate.registerForPushNotification(application: UIApplication.shared)
    }

}
