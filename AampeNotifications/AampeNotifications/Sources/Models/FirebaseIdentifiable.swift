//
//  FirebaseIdentifiable.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 19/01/24.
//

import Foundation

protocol FirebaseIdentifiable: Hashable, Codable {
    var id: String { get set }
}
