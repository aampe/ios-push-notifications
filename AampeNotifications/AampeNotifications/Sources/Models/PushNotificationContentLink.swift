//
//  PushNotificationContentLink.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 27/12/23.
//

import Foundation

struct PushNotificationContentLink {
    let type: PushNotificationContentLinkType
    let contentId: String?
    let contentName: String?

    init?(userInfo: [AnyHashable: Any]) {
        guard let type = userInfo["type"] as? String else {
            return nil
        }
        guard let linkType = PushNotificationContentLinkType(rawValue: type) else {
            return nil
        }

        self.type = linkType
        self.contentId = userInfo["contentId"] as? String
        self.contentName = userInfo["contentName"] as? String
    }

    func screenLink() -> PushNotificationScreenLink? {
        switch contentName {
        case "Product":
            return .product(id: contentId ?? "")
        default:
            return nil
        }
    }
}
