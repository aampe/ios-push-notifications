//
//  User.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 19/01/24.
//

import Foundation

struct User: FirebaseIdentifiable {
    var id: String
    var pushToken: String
    var name: String
    var email: String
}
