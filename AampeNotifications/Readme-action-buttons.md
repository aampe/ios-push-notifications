# What are Action Buttons in Notification?

Action buttons are a vital part of the iOS notification system, allowing users to interact with notifications directly from the home screen. According to Apple documentation, these buttons can be used to trigger specific actions within an app without needing to open the app itself.

The User Notifications framework provides several types of actions, including:

- Generic Actions: These can be used for any purpose and are often used to open the app or perform a task.
- Text Input Actions: These allow the user to respond with text directly from the notification.
- Destructive Actions: These are used for actions that might change or delete data and are usually styled to warn the user.

`Starting from iOS 15, Apple has taken notification actions a step further by allowing developers to include images in these buttons.`

![action button](../Images/18.png)

# How to add notification actions

For this we need to register a so-called notification category in which we can register the buttons. Every notification within that category will show the registered buttons beneath the content when being displayed in detail.

we have to register notification categories containing the actions at launch time within the main app.

```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
          UNUserNotificationCenter.current().delegate = self
          Messaging.messaging().delegate = self
          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("OPEN", comment: "open"), options: UNNotificationActionOptions.foreground)
        let deafultCategory = UNNotificationCategory(identifier: "myNotificationCategory", actions: [openAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories(Set([deafultCategory]))
        application.registerForRemoteNotifications()
        return true
    }
```
This will register an “OpenNotification” action button for all notifications in the “myNotificationCategory” category.

`It’s important to give your actions a unique identifier as it’s the only way to distinguish one action from another.`

## Handling the actionable button press callback

with in the `userNotificationCenter(_:didReceive:withCompletionHandler:)` method of `UNUserNotificationCenterDelegate` you can find the selected action and perfrom required operation.

Compare the actionIdentifier of response with your actionable button Identifier.

![action](../Images/19.png)
