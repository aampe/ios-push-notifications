# Guide to integrate Firebase Cloud Messaging (FCM) in your iOS application.

### NOTE: This is a sample guid repo to integrate Firebase Cloud Messaging (FCM) in your iOS application using Swift with UIKit. If you are working with Objective-C or SwiftUI, you can still take it as a reference to integrate push notifications into your iOS application.

## STEP 1 : Create a new iOS app using Xcode with push notification capability enabled 
First of all, let’s create a new Xcode Project.
Navigate to your project and select the target that you wish to add push notification for.

![push capability](../Images/1.png)

Click on Capability and add Background Modes and Push Notifications.
![enable push](../Images/2.png)

![enable push](../Images/3.png)

If you can not find Push Notifications, it might because you are on a free Developer Program account. Switch to a paid one, and you should see it showing up.
Check Remote notifications.

![enable push](../Images/4.png)

If you have Automatically mange signing selected like below, you should see your app showing up with push notification service enabled on your developer account identifiers list.

![enable auto sign](../Images/10.png)

![enable push](../Images/5.png)

## STEP 2 : APNs authentication key
Having push notifications enabled for our Xcode project, we can now move onto creating a private key with APNs enabled.
This new way of using private key to access and authenticate communication with APNs is a lot easier to use compare to the old way of setting up for push notification, ie: signing your app, getting an identity ID, and managing your certification.

You can use this APNs signing private key for multiple apps. The signing key works for both the development and production environments and the signing key doesn’t expire.

Follow the steps below to create a [private key](https://developer.apple.com/help/account/manage-keys/create-a-private-key)

1. In Certificates, Identifiers & Profiles, click Keys in the sidebar, then click the add button (+) on the top left.
2. Enter a unique key name.
3. Select the checkbox next to the Push Notifications, then click Continue.
4. Review the key configuration, then click Confirm.
5. Click Download to generate and download the key.

After that, we will get the key identifier for the private key above.
1. In Certificates, Identifiers & Profiles, click Keys in the sidebar.
2. On the right, select the private key to view details.
3. The key identifier appears below the key name.

## STEP 3 : Firebase Set Up
Create a new Firebase Project
Creating a new project is really straight forward, we basically go to the [Firebase console](https://console.firebase.google.com/), and click Add project.
If you would like to know your app performance, you can choose to set up Google Analytics for your project, but everything would work fine without it.

# Register App with Firebase

Now having our private key with APNs enabled and the key identifier for the app as well as the Firebase project created, let’s add our app to the Firebase project we created above.
1. Go to the [Firebase console](https://console.firebase.google.com).
2. In the center of the project overview page, click the iOS+ icon to launch the setup workflow. If you've already added an app to your Firebase project, click Add app to display the platform options.
3. Enter the app info and click on Register.
![register firebase](../Images/11.png)

Reference : https://firebase.google.com/docs/ios/setup#register-app

# Add Config file to Project
After entering the app info, you will be able to get your `GoogleService-Info.plist` which is your Firebase Apple platforms config file (GoogleService-Info.plist).
Move this config file into the root of the Xcode project and add the config file to all targets.
Reference: https://firebase.google.com/docs/ios/setup#add-config-file

# Add Firebase SDKs
We will be using Swift Package Manager here but you an use Cocoapoda or Carthage if you like.
1. In Xcode, navigate to File > Add Packages.
2. Enter the package URL: https://github.com/firebase/firebase-ios-sdk
3. Select latest SDK version
4. Choose the Firebase libraries you would like to add. FirebaseMessaging is needed for Push Notification.
Reference: https://firebase.google.com/docs/ios/setup#add-sdks

# Upload Private Key to your Firebase Project
Inside the project in the Firebase console, select the gear icon, select Project Settings, and then select the Cloud Messaging tab.
In APNs authentication key under iOS app configuration, click the Upload button. Open your private key file and add the key Identifier for it. Click Upload to finish.

## STEP4 : Initialize Firebase in App
In your AppDelegate , add the following imports at top.
```
import FirebaseCore
import FirebaseMessaging
import UserNotifications
```
We will then register for remote notifications in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)` like following.
```
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]

        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
            (granted, error) in
            guard granted else { return }
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
        }
        return true
    }
```
This will show a permission dialog on first run. If you would like to show the dialog at a some other time, move this registration accordingly.
You can access the registration token via FIRMessagingDelegate'smessaging:didReceiveRegistrationToken: method.
```
func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {    
      print("Firebase registration token: \(String(describing: fcmToken))")
}
```
Here is where you might want to send your user data to your backend server.
Also, below are the cases when a register token might change so you might want to update your user database accordingly.
- The app is restored on a new device
- The user uninstalls/reinstall the app
- The user clears app data.

Putting everything together, our AppDelegate will look like following.

```

import UIKit
import FirebaseCore
import FirebaseMessaging
import UserNotifications


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerForPushNotifications(application: application)
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}



extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    /// UNUserNotificationCenterDelegate method to provide device token if user allow the permission.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
    }

    /// helper function to register for push notification 
    private func registerForPushNotifications(application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]

        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
            (granted, error) in
            guard granted else { return }
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
        }
    }
    
    
}

extension AppDelegate: MessagingDelegate {
    /// This callback is fired at each app startup and whenever a new token is generated.
    /// If necessary send token to application server.
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {    
      print("Firebase registration token: \(String(describing: fcmToken))")
    // store FCM token on your server if required
}
```
# NOTE: - You might want to create separate file or extension to handle push notification related things to do more modularise work. You can see example work (`FirebaseMessagingClient.swift`) in this repository.

# Testing

In the Firebase console, open the [Messaging page](https://console.firebase.google.com/u/0/project/_/messaging) and select Create your first campaign. Send a test message following the steps below.
1. Select Firebase Notification messages and select Create
2. Enter the message text
3. Select Send test message from the right pane.
4. In the field labeled Add an FCM registration token, enter the registration token you obtained in a previous section when you run your app.
5. Select Test.

You would see your push notification showing up on your Device!