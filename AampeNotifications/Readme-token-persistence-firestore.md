## This guide is an extension to Readme file to provide some context on how we can use Firebase's Firestore database to persists FCM token.

# Prerequisite : - 
For the purposes of this guide, the following assumptions are in place:
1. You’ve chosen `Cloud Firestore` as your database (although similar tactics can apply to Realtime Database)
2. You’re familiar with or using async/await with iOS 15 or newer
3. You’ve created your `Firebase` project in the console
4. You’ve generated and imported the `GoogleService-Info.plist` in Xcode
5. You’ve added Firebase and `FirebaseFirestoreSwift` through some dependency manager (SPM, CocoaPods, etc…)
6. You’ve configured `Firebase` within your `AppDelegate`

# Cloud Firestore
Cloud Firestore’s data model is based on `NoSQL Document-Oriented Database`. The data model may look like a key-value based database in Firebase Realtime Database, but Firestore has more advanced operations, more data types and improved performance. Firestore’s data model looks like this:
![Firebase Document](../Images/6.png)

# Enable Firestore Database 
Open up the Firebase and click on the Firestore Database option that is under the “Develop” section on the left panel as shown in the below image.
![Firebase database](../Images/7.png)

After that, you will be given an option to use Cloud Firestone or Realtime Database. I’ve used Cloud Database. You can also go through the article based in which database solution could be the right fit for you.
The Cloud Firestone will present you an image as below:
![Firebase cloud](../Images/8.png)
We will start in the “TEST MODE.” A test mode will not require additional setting up permission, and would also help us in quickly developing an application.
Click “Enable” for setting up Firebase in your Cloud Firestone.

# Save data to Firestore
To get the default instance of a Firestore database, declare the variable with type Firestore and call the firestore method.
```
var database: Firestore!

func viewDidLoad() {
    super.viewDidLoad()

    database = Firestore.firestore()
}
```
firestore will return a default (singleton) Firestore database instance, and it will return the same instance wherever you call it. To add new data, first, we need to have a collection and a document inside that collection. 
You can use this instance to interact with your Firebase Collections (database).
```
database.collection("animal").document("bird").setData([
    "name": "Peacock",
    "type": "Herbivore",
    "colors": ["Green", "White", "Blue", "Black"]
])
```
I have creates a service class called`FirebaseService` for Firebase related functions.
```
import Foundation
import Firebase
import FirebaseFirestoreSwift
import Foundation

enum Collections: String {
    case users = "users"
}

/// Very important - to use this class we use Firestore in the native mode. NOT in the cloud Firestore mode.
class FirebaseService {
    static let shared = FirebaseService()
    let database = Firestore.firestore()
    
    
    /// post function is used to created a new document(node) on the firebase database
    /// - Parameters:
    ///   - value: any value (firebase token in our case) to store on the firestore
    ///   - collection: a path (node) on the firestore
    /// - Returns: return error or success
    func post<T: FirebaseIdentifiable>(_ value: T, to collection: Collections) async -> Result<T, Error> {
        let ref = database.collection(collection.rawValue).document()
        var valueToWrite: T = value
        valueToWrite.id = ref.documentID
        do {
            try ref.setData(from: valueToWrite)
            return .success(valueToWrite)
        } catch let error {
            print("Error: \(#function) in collection: \(collection), \(error)")
            return .failure(error)
        }
    }
    
    /// put function is used to update the firebase token under user's uid
    /// - Parameters:
    ///   - value: any value (firebase token in our case) to store on the firestore
    ///   - collection: a path (node) on the firestore
    /// - Returns: return error or success
    func put<T: FirebaseIdentifiable>(_ value: T, to collection: Collections) async -> Result<T, Error> {
        let ref = database.collection(collection.rawValue).document(value.id)
        do {
            try ref.setData(from: value)
            return .success(value)
        } catch let error {
            print("Error: \(#function) in \(collection) for id: \(value.id), \(error)")
            return .failure(error)
        }
    }

    
    /// A helper functio to store firebase toien
    /// - Parameter token: firebase token
    func putFcmToken(token: String) async {
        guard let user = Auth.auth().currentUser else { return }
        _ = await put(
            User(
                id: user.uid,
                pushToken: token,
                name: user.displayName ?? "",
                email: user.email ?? ""
            ), to: .users)
    }
}
```
Here `FirebaseIdentifiable` is an protocol created for write a generic function to store values. We can created a simple `User` object the confirm to `FirebaseIdentifiable` protocol to store our FCM token at the Firebase (firestore). You can include as many as information. For this example we are storing `id`, `pushToken` i.e. FCM token, `name` and `email`.
```
protocol FirebaseIdentifiable: Hashable, Codable {
    var id: String { get set }
}

struct User: FirebaseIdentifiable {
    var id: String
    var pushToken: String
    var name: String
    var email: String
}
```
Whenever you get FCM token, you can call `putFcmToken` function from `FirebaseService` class to store your token at firestore. We receive FCM token in the `didReceiveRegistrationToken` function from `MessagingDelegate` like below-

```
/// This callback is fired at each app startup and whenever a new token is generated.
/// If necessary send token to application server.
/// - Parameter messaging: `Messaging` to define message object.
/// - Parameter fcmToken: `String?` to define FCM push notification token. That is used to send push message to client. You can save this token to own server to future use.
func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken else { return }
        print("Firebase registration token: \(String(describing: fcmToken))")
        /// Save APNS token on the firestore database. Please not  Firestore should be enabled in the `native` mode before try to save token.
        Task {
            await FirebaseService.shared.putFcmToken(token: fcmToken)
        }
    }
```

## Obtain FCM toekn for guest user and for logged-in user: - 
Most applicatios require user to authenticate before they could use the service and on the other hand some applicatios provide guest user facility (doesn't requires a user authentication).

# Logged-in user:
In this case an app requires user to authenticate before they could use the service and we need to store the FCM token along with the user id. So the most common approach would be - 
- Ask user for push notification permission after the successfully user login.
```
extension AppDelegate {
    /// Register for push notification.
    /// - Parameter application: `UIApplication` to define the notification auth options.
    /// - Parameter options: `UNAuthorizationOptions` to define the notification auth options. For eg. [.alert, .badge, .sound]
    func registerForPushNotification(
        application: UIApplication,
        options: UNAuthorizationOptions = [.alert, .badge, .sound]
    ) {        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        
        // Request for push notification authorization.
        notificationCenter.requestAuthorization(options: options) { authorized, error in
            if authorized {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        }
    }
}
```
- Once user authenticate the app to use push notification service, iOS delivers the `APNS` (Apple push notification service) token in the `didRegisterForRemoteNotificationsWithDeviceToken` funtion of the `UNUserNotificationCenterDelegate`.
```
// MARK: - User Notification Center Delegate
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    /// UNUserNotificationCenterDelegate method to provide device token if user allow the permission.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       Messaging.messaging().apnsToken = deviceToken
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
    }
    
    /// UNUserNotificationCenterDelegate method to fail if user don't allow or has any OS limitations.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for push notification with error:", error)
    }
}
```
- We provide the APNS token to Furebase Messaging service by setting it's `apnsToken` property as follow
`Messaging.messaging().apnsToken = deviceToken`

- Once the apns token is set we get the FCM token from Firebase Messaging service in the `func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?)` function from the `MessagingDelegate` and we can store the FCM token on our server (Firestore for this example).
```
// MARK: Firebase Messaging Deleagte
extension AppDelegate: MessagingDelegate {
    /// This callback is fired at each app startup and whenever a new token is generated.
    /// If necessary send token to application server.
    /// - Parameter messaging: `Messaging` to define message object.
    /// - Parameter fcmToken: `String?` to define FCM push notification token. That is used to send push message to client. You can save this token to own server to future use.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken else { return }
        print("Firebase registration token: \(String(describing: fcmToken))")
        /// Save APNS token on the firestore database. Please not  Firestore should be enabled in the `native` mode before try to save token.
        Task {
            await FirebaseService.shared.putFcmToken(token: fcmToken)
        }
    }
}
```
Note- Don't forget to register your messaging delegate while configuring your Firebase app.
```
FirebaseApp.configure()
Messaging.messaging().delegate = self
```
We usually do it in AppDelegate. But you can provide your own custom class for the delegate like we created `FirebaseMessagingClient` in this example.

# Guest user flow:
For the guest user the flow is almost same as we discussed above for logged-in user. The only difference is, in the guest flow we don't have to wait for user login. Instead, we can ask for user's permission for push notification on the `didFinishLaunchingWithOptions` of the AppDelegate. Like below-
```
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /// Configure firebase client
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerForPushNotification(application: application)
        return true
    }
}
```
rest of the work-flow is same as the logged-in user. That is-
- iOS would ask user's permission for push notification
- Application will receive apns token in the `didRegisterForRemoteNotificationsWithDeviceToken` of the AppDelegate
- We pass the apns to Firebase Messaging service as `Messaging.messaging().apnsToken = deviceToken`
- Messaging service provides FCM token in the `didReceiveRegistrationToken` function from `MessagingDelegate` (that is your AppDelegate)
- Store the FCM to firebase or any other server.

If you follow all the steps correctly, you should see your data being saved on the Firestore database as shown in the image below - 

![Firebase collection](../Images/9.png)
