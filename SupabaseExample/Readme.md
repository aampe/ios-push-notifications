# Supabase Swift Integration Guide
The Supabase Swift SDK is a client-side library that enables iOS and macOS developers to interact with Supabase services seamlessly. It provides a range of functionalities from authentication to real-time database updates, making it a powerful tool for building robust applications with Supabase as the backend.

## STEP 1: Create new project on Supabase
1. Login to your Supabase account and go to [project](https://supabase.com/dashboard/projects)
2. Click on `New project` button.
3. Fill the required informations like Organisation, name, etc and click on `Create new project` like below image

![Create project](../Images/12.png)

## STEP 2: Get API keys and secrets 
- After project is created successfully, click on the gear icon (project settings) on the bottom left corner of the page and select API from the menu.

![copy secrets](../Images/13.png)

- From the above page copy the `URL` (fond under Project URL heading) and `anon` key from Project API keys section. We will need these values while initialising Supabase swift client.

## STEP 3: Create database tables
- Select `Table editor` from the left menu and click on the `create new table` button.

![Create table](../Images/14.png)

- Fill the table name like name and description 
- Create all the columns like 
 - id
 - name
 - email
 - pushToken
 - platform
- Click on `Save` button.
![create table](../Images/15.png)

## STEP 4: Set security rules
Once table is created we need to set security rules.
- Click on `No active RLS police` as shown below- 

![set policy](../Images/16.png)

- Click on `New policy` and clcik on `Get started quickly`

![set policy](../Images/17.png)

- Select the policy template according to your project needs

## STEP 5: Add Supabase Swift client
We will be using Swift Package Manager here but you an use Cocoapoda or Carthage if you like.

- In Xcode, navigate to File > Add Packages.
- Enter the package URL: https://github.com/supabase-community/supabase-swift.git
- Select latest SDK version
Choose the Supabase libraries you would like to add. Storage is only required for our purposes as we only interested in token storing on Supabase.

## STEP 6: Supabase clicnt setup
- Initialise Supabase client by using project `UR`L and `anon` key. We copied these value in step 2 above
```
/// Create a single supabase client for interacting with your database
let client = SupabaseClient(supabaseURL: URL(string: "https://xyzcompany.supabase.co")!, supabaseKey: "public-anon-key")
```
Now you can use Supabase Swift library's functions to store/update your FCM token. Like- 
- Insert
```
struct User: Encodable {
  let id: Int
  let token: String
}

let user = User(id: 1, token: "qwertyuiosdfghjxcvbnmuiuifghjt67890fghj")

try await supabase.database
  .from("users")
  .insert(user)
  .execute()
```
- update
```
try await supabase.database
  .from("users")
  .update(["token": "vghgqwertyuiosjhbjfdfghjxcvbnmuiuifghjt67890fghj"])
  .eq("id", value: 1)
  .execute()
```

- Fetch 
```
struct User: Encodable {
  let id: Int
  let token: String
}

let users: [User] = try await supabase.database
  .from("users")
  .select()
  .eq("id", value: 1)
  .execute()
  .value
```
## STEP 7: Enable push notifications 
Follow the guidelines for enable push notifications in your iOS app using Firebase Cloud Messaging (FCM) services.
[FCM guide](https://gitlab.com/aampe/ios-push-notifications/-/blob/main/AampeNotifications/Readme.md?ref_type=heads)

## STEP 8: Store FCM token on Supabase
- Whenever a user Login to the app or enter as a guest user we can query Supabase database if a user is already present. If present we can update the user with the new FCM token else insert a new entry.
- On any subsequent app launch when we get a new FCM token we can simply update our users table with the new FCM token.

I hvae created a service class for all the Supabase functionalities called `SupabaseService`
```import Foundation
import Supabase

enum DatabaseTable: String {
    case users = "users"
}

enum DatabaseResult<Success> {
    case success(Success)
    case failure(Error)
    case noRecord
}

class SupabaseService {
    static let shared = SupabaseService()

    private let client = SupabaseClient(
        supabaseURL: URL(string: "https://fwuhgnjxhlrjeweuuxzl.supabase.co")!,
        supabaseKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImZ3dWhnbmp4aGxyamV3ZXV1eHpsIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDc5MDkwMTQsImV4cCI6MjAyMzQ4NTAxNH0.pK2wpxJTaazLyAintYdkuClYayafW5E5is9otEmLkBA"
    )

    private func fetch<T: SupabaseIdentifiable>(for id: String, table: DatabaseTable) async -> DatabaseResult<T> {
        do {
            let result: [T] = try await client.database
                .from(table.rawValue)
                .select()
                .eq("id", value: id)
                .execute()
                .value

            if let value = result.first {
                return .success(value)
            }
            return .noRecord
        } catch {
            return .failure(error)
        }
    }

    private func insert<T: SupabaseIdentifiable>(_ value: T, table: DatabaseTable) async -> Bool {
        do {
            try await client.database.from(table.rawValue).insert(value).execute()
            return true
        } catch {
            print("Failed to insert data with error:", error)
            return false
        }
    }

    private func update<T: SupabaseIdentifiable>(_ value: T, table: DatabaseTable) async -> Bool {
        do {
            try await client.database
                .from(table.rawValue)
                .update(value)
                .eq("id", value: value.id)
                .execute()
            return true
        } catch {
            return false
        }
    }

    private func update(for id: String, value: [String: String], table: DatabaseTable) async -> Bool {
        do {
            try await client.database
                .from(table.rawValue)
                .update(value)
                .eq("id", value: id)
                .execute()
            return true
        } catch {
            print("Failed to update data with error:", error)
            return false
        }
    }
}

extension SupabaseService {
    static func getUser(byId: String) async -> DatabaseResult<User> {
        await SupabaseService.shared.fetch(for: byId, table: .users)
    }

    static func insert(user: User) async -> Bool {
        await SupabaseService.shared.insert(user, table: .users)
    }

    static func update(user: User) async -> Bool {
        await SupabaseService.shared.update(user, table: .users)
    }

    static func insertOrUpdateUser(user: User) async -> Bool {
        let result = await getUser(byId: user.id)
        switch result {
        case .success:
            return await update(user: user)
        case .noRecord:
            return await insert(user: user)
        default:
            return false
        }
    }

    static func insertIfNotFound(user: User) async -> Bool {
        let result = await getUser(byId: user.id)
        switch result {
        case .noRecord:
            return await insert(user: user)
        default:
            return false
        }
    }

    static func updatePushToken(for id: String, token: String) async -> Bool {
        await shared.update(for: id, value: ["pushToken": token], table: .users)
    }
}
```
And use `insertOrUpdateUser` function on user Login or guest user entry to uodate or insert FCM token as follows-
```
Task {
        _ = await SupabaseService.insertOrUpdateUser(user: user)
    }
```
And use `updatePushToken` whenever we get new FCM token on app launch.
```
extension FirebaseMessagingClient: MessagingDelegate {
    
    /// This callback is fired at each app startup and whenever a new token is generated.
    /// If necessary send token to application server.
    /// - Parameter messaging: `Messaging` to define message object.
    /// - Parameter fcmToken: `String?` to define FCM push notification token. That is used to send push message to client. You can save this token to own server to future use.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken, let currentUserId = AuthManager.currentUserId else { return }
        print("Firebase registration token: \(String(describing: fcmToken))")
        /// Save FCM token on the supabase database. 
        Task {
            _ = await SupabaseService.updatePushToken(for: currentUserId, token: fcmToken)
        }
    }
}
```

References:
Supabase Swift client- https://supabase.com/docs/reference/swift/initializing
Supabase docs: https://supabase.com/docs/guides/storage
Firebase FCM: https://firebase.google.com/docs/ios/setup#register-app
