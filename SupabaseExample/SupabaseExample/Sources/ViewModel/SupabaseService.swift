//
//  SupabaseService.swift
//  SupabaseExample
//
//  Created by Bharat Lal on 14/02/24.
//

import Foundation
import Supabase

enum DatabaseTable: String {
    case users = "users"
}

enum DatabaseResult<Success> {
    case success(Success)
    case failure(Error)
    case noRecord
}

class SupabaseService {
    static let shared = SupabaseService()

    private let client = SupabaseClient(
        supabaseURL: URL(string: "https://fwuhgnjxhlrjeweuuxzl.supabase.co")!,
        supabaseKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImZ3dWhnbmp4aGxyamV3ZXV1eHpsIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDc5MDkwMTQsImV4cCI6MjAyMzQ4NTAxNH0.pK2wpxJTaazLyAintYdkuClYayafW5E5is9otEmLkBA"
    )

    private func fetch<T: SupabaseIdentifiable>(for id: String, table: DatabaseTable) async -> DatabaseResult<T> {
        do {
            let result: [T] = try await client.database
                .from(table.rawValue)
                .select()
                .eq("id", value: id)
                .execute()
                .value

            if let value = result.first {
                return .success(value)
            }
            return .noRecord
        } catch {
            return .failure(error)
        }
    }

    private func insert<T: SupabaseIdentifiable>(_ value: T, table: DatabaseTable) async -> Bool {
        do {
            try await client.database.from(table.rawValue).insert(value).execute()
            return true
        } catch {
            print("Failed to insert data with error:", error)
            return false
        }
    }

    private func update<T: SupabaseIdentifiable>(_ value: T, table: DatabaseTable) async -> Bool {
        do {
            try await client.database
                .from(table.rawValue)
                .update(value)
                .eq("id", value: value.id)
                .execute()
            return true
        } catch {
            return false
        }
    }

    private func update(for id: String, value: [String: String], table: DatabaseTable) async -> Bool {
        do {
            try await client.database
                .from(table.rawValue)
                .update(value)
                .eq("id", value: id)
                .execute()
            return true
        } catch {
            print("Failed to update data with error:", error)
            return false
        }
    }
}

extension SupabaseService {
    static func getUser(byId: String) async -> DatabaseResult<User> {
        await SupabaseService.shared.fetch(for: byId, table: .users)
    }

    static func insert(user: User) async -> Bool {
        await SupabaseService.shared.insert(user, table: .users)
    }

    static func update(user: User) async -> Bool {
        await SupabaseService.shared.update(user, table: .users)
    }

    static func insertOrUpdateUser(user: User) async -> Bool {
        let result = await getUser(byId: user.id)
        switch result {
        case .success:
            return await update(user: user)
        case .noRecord:
            return await insert(user: user)
        default:
            return false
        }
    }

    static func insertIfNotFound(user: User) async -> Bool {
        let result = await getUser(byId: user.id)
        switch result {
        case .noRecord:
            return await insert(user: user)
        default:
            return false
        }
    }

    static func updatePushToken(for id: String, token: String) async -> Bool {
        await shared.update(for: id, value: ["pushToken": token], table: .users)
    }
}
