//
//  Router.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import Foundation
import UIKit

class Router {
    private var window: UIWindow?

    static let shared = Router()
    private init() {}

    static var navigation: UINavigationController? {
        shared.window?.rootViewController as? UINavigationController
    }

    static func setWindow(_ window: UIWindow) {
        shared.window = window
    }

    static func changeWindowRoot(_ viewController: UIViewController) {
        let navigation = UINavigationController(rootViewController: viewController)
        shared.window?.rootViewController = navigation
        shared.window?.makeKeyAndVisible()
    }

    static func loadInitialScreen(isLoggedIn: Bool) {
        if isLoggedIn {
            let viewController: HomeViewController = .create(with: .main)
            changeWindowRoot(viewController)
        } else {
            let viewController: SignInViewController = .create(with: .main)
            changeWindowRoot(viewController)
        }
    }

    static func push(_ viewController: UIViewController) {
        navigation?.pushViewController(viewController, animated: true)
    }

    static func present(_ viewController: UIViewController) {
        navigation?.present(viewController, animated: true)
    }
}
