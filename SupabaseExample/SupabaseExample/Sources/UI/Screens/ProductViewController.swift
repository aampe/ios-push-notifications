//
//  ProductViewController.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import UIKit

class ProductViewController: UIViewController {
    @IBOutlet private weak var messageLabel: UILabel!
    
    var contentId: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        messageLabel.text = "Product Details: \(contentId)"
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
