//
//  PushNotificationContentLinkType.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import Foundation

enum PushNotificationContentLinkType: String, Codable {
    case screen
    case openOtherApp
}
