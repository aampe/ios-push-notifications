//
//  PushNotificationScreenLink.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import Foundation

enum PushNotificationScreenLink {
    case product(id: String)
}
