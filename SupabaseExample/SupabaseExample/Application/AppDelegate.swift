//
//  AppDelegate.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 25/12/23.
//

import UIKit
import GoogleSignIn

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /// Configure firebase client
        FirebaseMessagingClient.share.configure()
        if AuthManager.isLoggedIn {
            /// Register for remote notification
            registerForPushNotification(application: application)
        }
        return true
    }
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
      return GIDSignIn.sharedInstance.handle(url)
    }
}
