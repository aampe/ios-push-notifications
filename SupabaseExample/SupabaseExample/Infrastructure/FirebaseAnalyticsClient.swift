//
//  FirebaseAnalyticsClient.swift
//  AampeNotifications
//
//  Created by Bharat Lal on 24/01/24.
//

import Foundation
import FirebaseAnalytics

class FirebaseAnalyticsClient {
    class func setUser(id: String, name: String, email: String) {
        Analytics.setUserID(id)
        Analytics.setUserProperty("name", forName: name)
        Analytics.setUserProperty("email", forName: email)
    }

    class func logEvent(_ event: Event) {
        Analytics.logEvent(event.name, parameters: event.params)
    }
}

extension FirebaseAnalyticsClient {
    enum Event {
        case screenLink(name: String, contentId: String)

        var name: String {
            switch self {
            case .screenLink:
                return "notification_link"
            }
        }
        
        var params: [String: Any] {
            switch self {
            case let .screenLink(name, contentId):
                return [
                    "name": name,
                    "contentId": contentId
                ]
            }
        }
    }
}
